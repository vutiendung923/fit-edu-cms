package com.example.fitedu.sys.controller;
import com.example.fitedu.common.ResponseEntity;
import com.example.fitedu.common.ResponseTypes;
import com.example.fitedu.common.Roles;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.sys.service.CustomerService;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Valid
@Component
@RolesAllowed(Roles.admin)
@Path("customer")
public class CustomerController {

    @CurrentRequestId
    private String requestId;

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GET
    public Response getListCustomer(@QueryParam("pageIndex") Integer pageIndex,
                              @QueryParam("pageSize") Integer pageSize,
                              @QueryParam("fullName") String fullName,
                              @QueryParam("userName") String userName,
                              @QueryParam("custCode") String custCode) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                customerService.getListCustomer(pageIndex, pageSize, fullName,
                        userName, custCode))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailCustomer(@QueryParam("custId") Integer custId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                customerService.getDetail(custId))).build();
    }
}
