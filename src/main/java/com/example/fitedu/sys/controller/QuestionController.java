package com.example.fitedu.sys.controller;

import com.example.fitedu.common.ResponseEntity;
import com.example.fitedu.common.ResponseTypes;
import com.example.fitedu.common.Roles;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.sys.service.QuestionService;
import com.example.fitedu.sys.service.dto.question.QuestionDto;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@RolesAllowed(Roles.admin)
@Path("question")
public class QuestionController {

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @CurrentRequestId
    private String requestId;

    @GET
    public Response getListQuestion(@QueryParam("pageIndex") Integer pageIndex,
                                    @QueryParam("pageSize") Integer pageSize,
                                    @QueryParam("name") String name) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                questionService.getListQuestion(pageIndex, pageSize, name))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailQuestion(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                questionService.getDetailQuestion(id))).build();
    }

    @POST
    @Path("add")
    public Response addQuestion(QuestionDto questionDto) {
        questionService.addQuestion(questionDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateQuestion(QuestionDto questionDto) {
        questionService.updateQuestion(questionDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteQuestion(@QueryParam("id") Integer id) {
        questionService.deleteQuestion(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
