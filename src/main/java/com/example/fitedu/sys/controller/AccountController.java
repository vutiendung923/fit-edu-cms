package com.example.fitedu.sys.controller;

import com.example.fitedu.common.ResponseEntity;
import com.example.fitedu.common.ResponseTypes;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.sys.service.AccountService;
import com.example.fitedu.sys.service.dto.account.LoginDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("account")
public class AccountController {
    @CurrentRequestId
    private String requestId;

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Path("register")
    @POST
    public Response register(LoginDto loginDto) {
        accountService.register(loginDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @Path("login")
    @POST
    public Response loginDto(LoginDto loginDto) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                accountService.login(requestId, loginDto))).build();
    }
}
