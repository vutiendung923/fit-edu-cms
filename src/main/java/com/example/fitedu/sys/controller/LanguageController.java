package com.example.fitedu.sys.controller;

import com.example.fitedu.common.ResponseEntity;
import com.example.fitedu.common.ResponseTypes;
import com.example.fitedu.common.Roles;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.sys.service.LanguageService;
import com.example.fitedu.sys.service.dto.language.LanguageDto;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Path("language")
@RolesAllowed(Roles.admin)
public class LanguageController {
    @CurrentRequestId
    private String requestId;

    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GET
    @Path("list")
    public Response getListLang(@QueryParam("pageIndex") Integer pageIndex,
                                @QueryParam("pageSize") Integer pageSize,
                                @QueryParam("name") String name) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                languageService.getListLang(pageIndex, pageSize, name))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailLang(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                languageService.getDetailLang(id))).build();
    }

    @POST
    @Path("add")
    public Response addLang(LanguageDto languageDto) {
        languageService.addLang(languageDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateLang(LanguageDto languageDto) {
        languageService.updateLang(languageDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteLang(@QueryParam("id") Integer id){
        languageService.deleteLang(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
