package com.example.fitedu.sys.controller;

import com.example.fitedu.common.Auth;
import com.example.fitedu.common.ResponseEntity;
import com.example.fitedu.common.ResponseTypes;
import com.example.fitedu.common.Roles;
import com.example.fitedu.config.inject.CurrentAuth;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.sys.service.CoursesService;
import com.example.fitedu.sys.service.dto.courses.CoursesCategoryDto;
import com.example.fitedu.sys.service.dto.courses.CoursesDto;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@RolesAllowed(Roles.admin)
@Path("courses")
public class CoursesController {
    @CurrentRequestId
    private String requestId;

    @CurrentAuth
    private Auth auth;

    private final CoursesService coursesService;


    public CoursesController(CoursesService coursesService) {
        this.coursesService = coursesService;
    }

    @GET
    @Path("list")
    public Response getList(@QueryParam("pageIndex") Integer pageIndex,
                            @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("name") String name){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                coursesService.getList(pageIndex, pageSize, name))).build();
    }

    @GET
    @Path("detail")
    public Response getDetail(@QueryParam("id") Integer id){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                coursesService.getDetail(id))).build();
    }

    @POST
    @Path("add")
    public Response addCourses(CoursesDto coursesDto){
        coursesService.addCourses(auth, coursesDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateCourses(CoursesDto coursesDto){
        coursesService.updateCourses(auth, coursesDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response updateCourses(@QueryParam("id") Integer id){
        coursesService.deleteCourses(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("category/list")
    public Response getListCategory(@QueryParam("pageIndex") Integer pageIndex,
                                    @QueryParam("pageSize") Integer pageSize,
                                    @QueryParam("name") String name) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                coursesService.getListCategory(pageIndex, pageSize, name))).build();
    }

    @GET
    @Path("category/detail")
    public Response getDetailCategory(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                coursesService.getDetailCategory(id))).build();
    }

    @POST
    @Path("category/add")
    public Response addCoursesCategory(CoursesCategoryDto coursesCategoryDto){
        coursesService.addCoursesCategory(auth, coursesCategoryDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("category/update")
    public Response updateCoursesCategory(CoursesCategoryDto coursesCategoryDto){
        coursesService.updateCoursesCategory(auth, coursesCategoryDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("category/delete")
    public Response deleteCoursesCategory(@QueryParam("id") Integer id){
        coursesService.deleteCoursesCategory(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
