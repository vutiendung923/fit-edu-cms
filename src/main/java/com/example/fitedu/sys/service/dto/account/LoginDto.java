package com.example.fitedu.sys.service.dto.account;


import com.example.fitedu.sys.model.Account;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class LoginDto {

    private Integer id;

    @NotBlank
    @Length(min = 5)
    private String userName;

    @NotBlank
    @Length(min = 5)
    private String password;

    @Length(min = 5)
    private String pass_old;

    public LoginDto(Account account) {
        this.userName = account.getUserName();
        this.password = account.getPassword();
    }
}
