package com.example.fitedu.sys.service.impl;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.model.Customer;
import com.example.fitedu.sys.repository.CustomerRepo;
import com.example.fitedu.sys.root.Customer_;
import com.example.fitedu.sys.service.CustomerService;
import com.example.fitedu.sys.service.dto.customer.CustomerDto;
import com.example.fitedu.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;

    public CustomerServiceImpl(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Override
    @Transactional
    public PageData<CustomerDto> getListCustomer(Integer pageIndex, Integer pageSize, String custCode, String fullName, String userName) {
        Specification<Customer> specification = (root, cq, cb) ->(
                QueryUtils.buildLikeFilter(root, cb, fullName, Customer_.full_name)
        );
        Page<Customer> customerPage = customerRepo.findAll(specification, PageRequest.of(pageIndex-1, pageSize));
        List<CustomerDto> customerDtoList = customerPage.getContent()
                .stream()
                .map(customer -> CustomerDto.builder().customer(customer).build())
                .collect(Collectors.toList());
        return PageData.of(customerPage, customerDtoList);
    }

    @Override
    @Transactional
    public CustomerDto getDetail(Integer custId) {
        return CustomerDto.builder().customer(
                customerRepo.findById(custId).orElseThrow(RuntimeException::new)
        ).build();
    }
}
