package com.example.fitedu.sys.service;

import javax.servlet.http.HttpSession;

public interface SpamService {
    boolean spamPerSecond(HttpSession session);
}
