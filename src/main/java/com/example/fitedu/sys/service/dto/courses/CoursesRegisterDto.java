package com.example.fitedu.sys.service.dto.courses;

import com.example.fitedu.sys.model.Courses;
import com.example.fitedu.sys.model.CoursesRegister;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class CoursesRegisterDto {
    private Integer id;
    private String coursesName;
    private Date registerTime;

    public CoursesRegisterDto(CoursesRegister coursesRegister) {
        this.id = coursesRegister.getId();
        this.coursesName = coursesRegister.getCourses().getName();
        this.registerTime = coursesRegister.getRegisterTime();
    }
 }
