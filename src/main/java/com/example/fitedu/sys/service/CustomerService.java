package com.example.fitedu.sys.service;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.service.dto.customer.CustomerDto;

public interface CustomerService {
    PageData<CustomerDto> getListCustomer(Integer pageIndex, Integer pageSize,
                                          String custCode, String fullName, String userName);
    CustomerDto getDetail(Integer custId);
}
