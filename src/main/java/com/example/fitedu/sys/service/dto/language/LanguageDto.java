package com.example.fitedu.sys.service.dto.language;

import com.example.fitedu.sys.model.Language;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class LanguageDto {
    private Integer id;
    private String name;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;

        public Builder language(Language language) {
            this.id  = language.getId();
            this.name = language.getName();
            return this;
        }

        public LanguageDto build() {
            return new LanguageDto(id, name);
        }
    }
}
