package com.example.fitedu.sys.service.impl;

import com.example.fitedu.common.Auth;
import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.model.Courses;
import com.example.fitedu.sys.model.CoursesCategory;
import com.example.fitedu.sys.repository.CoursesCategoryRepo;
import com.example.fitedu.sys.repository.CoursesRepo;
import com.example.fitedu.sys.root.CoursesCategory_;
import com.example.fitedu.sys.root.Courses_;
import com.example.fitedu.sys.service.CoursesService;
import com.example.fitedu.sys.service.dto.courses.CoursesCategoryDto;
import com.example.fitedu.sys.service.dto.courses.CoursesDto;
import com.example.fitedu.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;

import javax.persistence.RollbackException;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CoursesImplService implements CoursesService {

    private final CoursesRepo coursesRepo;
    private final CoursesCategoryRepo coursesCategoryRepo;

    public CoursesImplService(CoursesRepo coursesRepo, CoursesCategoryRepo coursesCategoryRepo) {
        this.coursesRepo = coursesRepo;
        this.coursesCategoryRepo = coursesCategoryRepo;
    }

    @Override
    @Transactional
    public PageData<CoursesDto> getList(Integer pageIndex, Integer pageSize, String name) {
        Specification<Courses> specification = (root, cq, cb) -> (
                QueryUtils.buildLikeFilter(root, cb, name, Courses_.name)
                );
        Page<Courses> coursesPage = coursesRepo.findAll(specification, PageRequest.of(pageIndex-1,pageSize));
        List<CoursesDto> coursesDtoList = coursesPage.getContent().stream()
                .map(courses -> CoursesDto.builder().courses(courses).build())
                .collect(Collectors.toList());
        return PageData.of(coursesPage, coursesDtoList);
    }

    @Override
    @Transactional
    public CoursesDto getDetail(Integer id) {
        return CoursesDto.builder().courses(coursesRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }

    @Transactional
    @Override
    public void addCourses(Auth auth, CoursesDto coursesDto) {
        Courses courses = new Courses();
        courses.setName(coursesDto.getName());
        courses.setDescription(coursesDto.getDescription());
        courses.setPrice(coursesDto.getPrice());
        courses.setImage(coursesDto.getImage());
        courses.setCreateBy(auth.getUserName());
        courses.setCreateTime(new Date());
        coursesRepo.save(courses);
    }

    @Transactional
    @Override
    public void updateCourses(Auth auth, CoursesDto coursesDto) {
        Courses courses = coursesRepo.findById(coursesDto.getId()).orElseThrow(RuntimeException::new);
        courses.setName(coursesDto.getName());
        courses.setDescription(coursesDto.getDescription());
        courses.setPrice(coursesDto.getPrice());
        courses.setImage(coursesDto.getImage());
        courses.setUpdateBy(auth.getUserName());
        courses.setUpdateTime(new Date());
        coursesRepo.save(courses);
    }

    @Override
    public void deleteCourses(Integer id) {
        coursesRepo.deleteById(id);
    }

    @Override
    public PageData<CoursesCategoryDto> getListCategory(Integer pageIndex, Integer pageSize, String name) {
        Specification<CoursesCategory> specification = (root, cq, cb) ->(
                QueryUtils.buildLikeFilter(root, cb, name, CoursesCategory_.name)
        );
        Page<CoursesCategory> coursesCategoryPage = coursesCategoryRepo.findAll(specification,
                PageRequest.of(pageIndex-1, pageSize));
        List<CoursesCategoryDto> coursesCategoryDtoList = coursesCategoryPage.getContent()
                .stream()
                .map(coursesCategory -> CoursesCategoryDto.builder().coursesCategory(coursesCategory).build())
                .collect(Collectors.toList());
        return PageData.of(coursesCategoryPage, coursesCategoryDtoList);
    }

    @Override
    public CoursesCategoryDto getDetailCategory(Integer id) {
        return CoursesCategoryDto.builder().coursesCategory(coursesCategoryRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }

    @Override
    public void addCoursesCategory(Auth auth, CoursesCategoryDto coursesCategoryDto) {
        CoursesCategory coursesCategory = new CoursesCategory();
        coursesCategory.setName(coursesCategoryDto.getName());
        coursesCategoryRepo.save(coursesCategory);
    }

    @Override
    public void updateCoursesCategory(Auth auth, CoursesCategoryDto coursesCategoryDto) {
        Integer id = coursesCategoryDto.getId();
        CoursesCategory coursesCategory = coursesCategoryRepo.findById(id).orElseThrow(RuntimeException::new);
        coursesCategory.setName(coursesCategoryDto.getName());
        coursesCategoryRepo.save(coursesCategory);
    }

    @Override
    public void deleteCoursesCategory(Integer id) {
        coursesCategoryRepo.deleteById(id);
    }
}
