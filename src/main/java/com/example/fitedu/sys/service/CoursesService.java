package com.example.fitedu.sys.service;

import com.example.fitedu.common.Auth;
import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.model.CoursesCategory;
import com.example.fitedu.sys.service.dto.courses.CoursesCategoryDto;
import com.example.fitedu.sys.service.dto.courses.CoursesDto;

import java.util.Optional;

public interface CoursesService {
    PageData<CoursesDto> getList(Integer pageIndex, Integer pageSize, String name);
    CoursesDto getDetail(Integer id);
    void addCourses(Auth auth, CoursesDto coursesDto);
    void updateCourses(Auth auth, CoursesDto coursesDto);
    void deleteCourses(Integer id);
    PageData<CoursesCategoryDto> getListCategory(Integer pageIndex, Integer pageSize, String name);
    CoursesCategoryDto getDetailCategory(Integer id);
    void addCoursesCategory(Auth auth, CoursesCategoryDto coursesCategoryDto);
    void updateCoursesCategory(Auth auth, CoursesCategoryDto coursesCategoryDto);
    void deleteCoursesCategory(Integer id);
}
