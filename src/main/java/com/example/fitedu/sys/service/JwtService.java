
package com.example.fitedu.sys.service;


import com.example.fitedu.common.Auth;

public interface JwtService {
    String encode(Auth auth, int hourNumber);

    Auth decode(String jwt);

    boolean isExpiredToken(String token);
}

