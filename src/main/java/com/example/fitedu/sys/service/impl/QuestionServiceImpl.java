package com.example.fitedu.sys.service.impl;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.model.Courses;
import com.example.fitedu.sys.model.Question;
import com.example.fitedu.sys.repository.QuestionRepo;
import com.example.fitedu.sys.root.Question_;
import com.example.fitedu.sys.service.QuestionService;
import com.example.fitedu.sys.service.dto.question.QuestionDto;
import com.example.fitedu.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepo questionRepo;

    public QuestionServiceImpl(QuestionRepo questionRepo) {
        this.questionRepo = questionRepo;
    }

    @Override
    @Transactional
    public PageData<QuestionDto> getListQuestion(Integer pageIndex, Integer pageSize, String name) {
        Specification<Question> specification = (root, cq, cb) -> (
                QueryUtils.buildLikeFilter(root, cb, name, Question_.name));
        Page<Question> questionPage = questionRepo.findAll(specification, PageRequest.of(pageIndex-1, pageSize));
        List<QuestionDto> questionDtoList = questionPage.getContent()
                .stream()
                .map(question -> QuestionDto.builder().question(question).build())
                .collect(Collectors.toList());
        return PageData.of(questionPage, questionDtoList);
    }

    @Override
    @Transactional
    public QuestionDto getDetailQuestion(Integer id) {
        return QuestionDto.builder().question(
                questionRepo.findById(id).orElseThrow(RuntimeException::new)
        ).build();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addQuestion(QuestionDto questionDto) {
        Question question = new Question();
        if(questionDto.getCourses() != null) {
            question.setCourses(new Courses(questionDto.getCourses().getId()));
        }
        question.setName(questionDto.getName());
        questionRepo.save(question);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateQuestion(QuestionDto questionDto) {
        Integer id = questionDto.getId();
        Question question = questionRepo.findById(id).orElseThrow(RuntimeException::new);
        if(questionDto.getCourses() != null) {
            question.setCourses(new Courses(questionDto.getCourses().getId()));
        }
        question.setName(questionDto.getName());
        questionRepo.save(question);
    }

    @Override
    @Transactional
    public void deleteQuestion(Integer id) {
        questionRepo.deleteById(id);
    }
}
