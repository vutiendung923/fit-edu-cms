package com.example.fitedu.sys.service.impl;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.model.Language;
import com.example.fitedu.sys.repository.LanguageRepo;
import com.example.fitedu.sys.root.Language_;
import com.example.fitedu.sys.service.LanguageService;
import com.example.fitedu.sys.service.dto.language.LanguageDto;
import com.example.fitedu.utils.QueryUtils;
import org.glassfish.hk2.runlevel.RunLevelException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepo languageRepo;

    public LanguageServiceImpl(LanguageRepo languageRepo) {
        this.languageRepo = languageRepo;
    }

    @Override
    public PageData<LanguageDto> getListLang(Integer pageIndex, Integer pageSize, String name) {
        Specification<Language> specification = (root, cq, cb) -> (
                QueryUtils.buildLikeFilter(root, cb, name, Language_.name));
        Page<Language> languagePage = languageRepo.findAll(specification, PageRequest.of(pageIndex-1,pageSize));
        List<LanguageDto> languageDto = languagePage.getContent()
                .stream()
                .map(language -> LanguageDto.builder().language(language).build())
                .collect(Collectors.toList());
        return PageData.of(languagePage, languageDto);
    }

    @Override
    public LanguageDto getDetailLang(Integer id) {
        return LanguageDto.builder().language(languageRepo.findById(id)
        .orElseThrow(RunLevelException::new)).build();
    }

    @Override
    public void addLang(LanguageDto languageDto) {
        Language language = new Language();
        language.setName(languageDto.getName());
        languageRepo.save(language);
    }

    @Override
    public void updateLang(LanguageDto languageDto) {
        Integer id = languageDto.getId();
        Language language = languageRepo.findById(id).orElseThrow(RuntimeException::new);
        language.setName(languageDto.getName());
        languageRepo.save(language);
    }

    @Override
    public void deleteLang(Integer id) {
        languageRepo.deleteById(id);
    }
}
