package com.example.fitedu.sys.service;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.service.dto.question.QuestionDto;

public interface QuestionService {
    PageData<QuestionDto> getListQuestion(Integer pageIndex, Integer pageSize, String name);
    QuestionDto getDetailQuestion(Integer id);
    void addQuestion(QuestionDto questionDto);
    void updateQuestion(QuestionDto questionDto);
    void deleteQuestion(Integer id);
}
