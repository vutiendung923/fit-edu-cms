package com.example.fitedu.sys.service;

import com.example.fitedu.common.PageData;
import com.example.fitedu.sys.service.dto.language.LanguageDto;

public interface LanguageService {
    PageData<LanguageDto> getListLang(Integer pageIndex, Integer pageSize, String name);
    LanguageDto getDetailLang(Integer id);
    void addLang(LanguageDto languageDto);
    void updateLang(LanguageDto languageDto);
    void deleteLang(Integer id);
}
