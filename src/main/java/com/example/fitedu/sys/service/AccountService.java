
package com.example.fitedu.sys.service;

import com.example.fitedu.sys.service.dto.account.LoginDto;
import com.example.fitedu.sys.service.dto.account.LoginResponseDto;

public interface AccountService {
    void register(LoginDto loginDto);
    LoginResponseDto login(String requestId, LoginDto loginDto);
}


