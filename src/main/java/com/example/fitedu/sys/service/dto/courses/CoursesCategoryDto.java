package com.example.fitedu.sys.service.dto.courses;

import com.example.fitedu.sys.model.CoursesCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class CoursesCategoryDto {
    private Integer id;
    private String name;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;

        public Builder coursesCategory(CoursesCategory coursesCategory) {
            this.id = coursesCategory.getId();
            this.name = coursesCategory.getName();
            return this;
        }

        public CoursesCategoryDto build(){
            return new CoursesCategoryDto(id, name);
        }
    }
}
