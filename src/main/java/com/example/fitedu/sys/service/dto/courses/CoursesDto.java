package com.example.fitedu.sys.service.dto.courses;

import com.example.fitedu.sys.model.Courses;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@ToString
@Data
@AllArgsConstructor
public class CoursesDto {
    private Integer id;
    private String name;
    private String description;
    private Integer price;
    private String image;
    private String coursesCategory;
    private String updateBy;
    private String createBy;
    private Date updateTime;
    private Date createTime;

    public CoursesDto(Courses courses) {
        this.id = courses.getId();
        this.name = courses.getName();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private String description;
        private Integer price;
        private String image;
        private String coursesCategory;
        private String updateBy;
        private String createBy;
        private Date updateTime;
        private Date createTime;

        public Builder courses(Courses courses) {
            this.id  = courses.getId();
            this.name = courses.getName();
            this.description = courses.getDescription();
            this.price = courses.getPrice();
            this.image = courses.getImage();
            if(courses.getCoursesCategory() != null) {
                this.coursesCategory = courses.getCoursesCategory().getName();
            }
            this.updateBy = courses.getUpdateBy();
            this.createBy = courses.getCreateBy();
            this.updateTime = courses.getUpdateTime();
            this.createTime = courses.getCreateTime();
            return this;
        }

        public CoursesDto build() {
            return new CoursesDto(id, name, description, price, image, coursesCategory, updateBy, createBy,
                    updateTime, createTime);
        }
    }
}
