package com.example.fitedu.sys.service.dto.question;

import com.example.fitedu.sys.model.Courses;
import com.example.fitedu.sys.model.Question;
import com.example.fitedu.sys.service.dto.answer.AnswerDto;
import com.example.fitedu.sys.service.dto.courses.CoursesDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class QuestionDto {
    private Integer id;
    private String name;
    private CoursesDto courses;
    private List<AnswerDto> answer;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private CoursesDto courses;
        private List<AnswerDto> answer;

        public Builder question(Question question) {
            this.id = question.getId();
            this.name = question.getName();
            if(question.getAnswer() != null) {
                this.courses = new CoursesDto(question.getCourses());
            }
            if(question.getAnswer() != null) {
                this.answer = question.getAnswer().stream()
                        .map(answer1 -> AnswerDto.builder().answer(answer1).build())
                        .collect(Collectors.toList());
            }
            return this;
        }

        public QuestionDto build() {
            return new QuestionDto(id, name, courses, answer);
        }
    }
}
