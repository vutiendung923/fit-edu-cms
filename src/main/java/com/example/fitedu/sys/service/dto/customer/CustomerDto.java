package com.example.fitedu.sys.service.dto.customer;

import com.example.fitedu.sys.model.BaseEntity;
import com.example.fitedu.sys.model.CoursesRegister;
import com.example.fitedu.sys.model.Customer;
import com.example.fitedu.sys.service.dto.courses.CoursesRegisterDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class CustomerDto {
    private Integer custId;
    private String custCode;
    private String userName;
    private String password;
    private String fullName;
    private Integer age;
    private String phone;
    private String email;
    private String address;
    private String avatar;
    private String facebook;
    private String profession;
    private Date birthday;
    private List<CoursesRegisterDto> coursesRegister;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer custId;
        private String custCode;
        private String userName;
        private String password;
        private String fullName;
        private Integer age;
        private String phone;
        private String email;
        private String address;
        private String avatar;
        private String facebook;
        private String profession;
        private Date birthday;
        private List<CoursesRegisterDto> coursesRegister;
        private String createBy;
        private Date createTime;
        private String updateBy;
        private Date updateTime;

        public Builder customer(Customer customer) {
            this.custId = customer.getId();
            this.custCode = customer.getCustCode();
            this.userName = customer.getUserName();
            this.password = customer.getPassword();
            this.fullName = customer.getFullName();
            this.age = customer.getAge();
            this.phone = customer.getPhone();
            this.email = customer.getEmail();
            this.address = customer.getAddress();
            this.avatar = customer.getAvatar();
            this.facebook = customer.getFacebook();
            this.profession = customer.getProfession();
            this.birthday = customer.getBirthday();
            this.coursesRegister = customer.getCoursesRegister().stream()
                    .map(CoursesRegisterDto::new)
                    .collect(Collectors.toList());
            this.createBy = customer.getCreateBy();
            this.createTime = customer.getCreateTime();
            this.updateBy = customer.getCreateBy();
            this.updateTime = customer.getUpdateTime();
            return this;
        }

        public CustomerDto build() {
            return new CustomerDto(custId, custCode, userName, password, fullName, age,
                    phone, email, address, avatar, facebook, profession, birthday, coursesRegister,
                    createBy, createTime, updateBy, updateTime);
        }
    }
}
