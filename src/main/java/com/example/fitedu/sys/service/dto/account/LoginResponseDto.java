package com.example.fitedu.sys.service.dto.account;

import com.example.fitedu.sys.model.Account;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LoginResponseDto {
    private Object account;
    private String role;
    private String accessToken;
    private String refreshToken;
}
