package com.example.fitedu.sys.service.dto.answer;

import com.example.fitedu.sys.model.Answer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnswerDto {
    private Integer id;
    private Integer questionId;
    private String name;
    private Integer status;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private Integer questionId;
        private String name;
        private Integer status;

        public Builder answer(Answer answer) {
            this.id = answer.getId();
            this.questionId = answer.getQuestion().getId();
            this.name = answer.getName();
            this.status = answer.getStatus();
            return this;
        }

        public AnswerDto build() {
            return new AnswerDto(id, questionId, name, status);
        }
    }
}
