package com.example.fitedu.sys.service.impl;

import com.example.fitedu.common.Auth;
import com.example.fitedu.common.Roles;
import com.example.fitedu.exception.DuplicateExceptionUserName;
import com.example.fitedu.exception.NotFoundAccountException;
import com.example.fitedu.sys.model.Account;
import com.example.fitedu.sys.repository.AccountRepo;
import com.example.fitedu.sys.service.AccountService;
import com.example.fitedu.sys.service.JwtService;
import com.example.fitedu.sys.service.dto.account.LoginDto;
import com.example.fitedu.sys.service.dto.account.LoginResponseDto;
import com.example.fitedu.utils.CryptoUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final JwtService jwtService;

    public AccountServiceImpl(AccountRepo accountRepo, JwtService jwtService) {
        this.accountRepo = accountRepo;
        this.jwtService = jwtService;
    }

    @Override
    public void register(LoginDto loginDto) {
        Account account = new Account();
        String userName = loginDto.getUserName();
        if(accountRepo.existsAccountByUserName(userName)) {
            throw new DuplicateExceptionUserName(userName);
        }else {
            account.setUserName(loginDto.getUserName());
            account.setPassword(CryptoUtils.encodeMD5(loginDto.getPassword()));
            accountRepo.save(account);
        }
    }

    @Override
    public LoginResponseDto login(String requestId, LoginDto loginDto) {
        String userName = loginDto.getUserName();
        String password = loginDto.getPassword();
        Account account = accountRepo.findAccountByUserNameAndAndPassword(userName, CryptoUtils.encodeMD5(password))
                .orElseThrow(() -> new NotFoundAccountException());
        Auth auth = new Auth();
        auth.setId(account.getId());
        auth.setUserName(account.getUserName());
        auth.setRoles(Collections.singletonList(Roles.admin));
        String token = jwtService.encode(auth, 1);
        LoginResponseDto loginResponseDto = new LoginResponseDto();
        loginResponseDto.setAccount(account);
        loginResponseDto.setRole(Roles.student);
        loginResponseDto.setAccessToken(token);
        return loginResponseDto;
    }

}
