package com.example.fitedu.sys.model;
import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "courses_register")
@ToString
public class CoursesRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Date registerTime;

    @ManyToOne
    @JoinColumn(name = "courses_id")
    private Courses courses;

    @ManyToOne
    @JoinColumn(name = "cust_id")
    private Customer customer;
}
