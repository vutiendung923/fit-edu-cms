package com.example.fitedu.sys.model;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class BaseEntity {
    protected String updateBy;
    protected String createBy;
    protected Date updateTime;
    protected Date createTime;
}
