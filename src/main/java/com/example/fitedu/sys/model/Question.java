package com.example.fitedu.sys.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "question")
@ToString
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "courses_id")
    private Courses courses;

    @OneToMany(mappedBy = "question")
    private List<Answer> answer;
}
