package com.example.fitedu.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "courses")
@Data
@ToString
@NoArgsConstructor
public class Courses extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Integer price;

    @Column(name = "image")
    private String image;

   @OneToMany(mappedBy = "courses")
   private List<CoursesRegister> coursesRegister;

    @ManyToOne
    @JoinColumn(name = "courses_category_id")
    private CoursesCategory coursesCategory;

    @OneToMany(mappedBy = "courses")
    private List<QuestionCourses> questionCourses;

    @OneToMany(mappedBy = "courses")
    private List<Question> question;

    public Courses(Integer id) {
        this.id = id;
    }
}
