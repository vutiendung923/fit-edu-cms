package com.example.fitedu.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "customer")
@Data
@ToString
public class Customer extends BaseEntity {
    @Id
    private Integer id;
    private String custCode;
    private String userName;
    private String password;
    private String fullName;
    private Integer age;
    private String phone;
    private String email;
    private String address;
    private String avatar;
    private String facebook;
    private String profession;
    private Date birthday;

    @OneToMany(mappedBy = "customer")
    private List<CoursesRegister> coursesRegister;
}
