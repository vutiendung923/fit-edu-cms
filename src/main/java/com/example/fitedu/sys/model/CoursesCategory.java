package com.example.fitedu.sys.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "courses_category")
@ToString
@NoArgsConstructor
public class CoursesCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "coursesCategory")
    private List<Courses> courses;

    public CoursesCategory(Integer coursesCategoryId) {
    }
}
