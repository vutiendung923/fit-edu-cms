package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LanguageRepo extends JpaRepository<Language, Integer>, JpaSpecificationExecutor<Language> {
}
