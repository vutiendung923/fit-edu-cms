package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.CoursesCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CoursesCategoryRepo extends JpaRepository<CoursesCategory, Integer>, JpaSpecificationExecutor<CoursesCategory> {
}
