package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.QuestionCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface QuestionCoursesRepo extends JpaRepository<QuestionCourses, Integer>, JpaSpecificationExecutor<QuestionCourses> {
}
