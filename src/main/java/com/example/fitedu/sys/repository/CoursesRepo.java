package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.Courses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CoursesRepo extends JpaRepository<Courses, Integer>, JpaSpecificationExecutor<Courses> {
}
