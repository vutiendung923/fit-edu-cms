package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepo extends JpaRepository<Account, Integer> {
    boolean existsAccountByUserName(String userName);
    Optional<Account> findAccountByUserNameAndAndPassword(String userName, String password);
}
