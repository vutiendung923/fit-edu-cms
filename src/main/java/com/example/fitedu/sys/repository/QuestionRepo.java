package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface QuestionRepo extends JpaRepository<Question, Integer>, JpaSpecificationExecutor<Question> {
}
