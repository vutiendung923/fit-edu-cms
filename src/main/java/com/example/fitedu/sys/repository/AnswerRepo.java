package com.example.fitedu.sys.repository;

import com.example.fitedu.sys.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AnswerRepo extends JpaRepository<Answer, Integer>, JpaSpecificationExecutor<Answer> {
}
