package com.example.fitedu.common;

public interface Roles {
  String student = "student";
  String admin = "admin";
  String viewer = "viewer";
}
