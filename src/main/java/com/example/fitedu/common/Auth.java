package com.example.fitedu.common;

import com.example.fitedu.sys.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class Auth {
    private Integer id;
    private String userName;
    private List<String> roles;
}
