package com.example.fitedu.common;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class PageData<T> {
    private Integer pageIndex;
    private Integer pageSize;
    private Integer totalPages;
    private Integer totalRecords;
    private Integer beginIndex;
    private Integer endIndex;
    private List<T> dataPage;

    public static <T> PageData<T> of(Page page, List<T> list) {
        PageData<T> pageData = new PageData<>();
        pageData.setPageIndex(page.getPageable().getPageNumber() + 1);
        pageData.setPageSize(page.getPageable().getPageSize());
        pageData.setBeginIndex(Math.toIntExact(page.getPageable().getOffset()));
        pageData.setEndIndex(Math.toIntExact(page.getPageable().getOffset() + page.getNumberOfElements()));
        pageData.setTotalPages(page.getTotalPages());
        pageData.setTotalRecords(Math.toIntExact(page.getTotalElements()));
        pageData.setDataPage(list);
        return pageData;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", totalPages=" + totalPages +
                ", totalRecords=" + totalRecords +
                ", beginIndex=" + beginIndex +
                ", endIndex=" + endIndex +
                ", dataPage.size=" + (dataPage == null ? null : dataPage.size()) +
                '}';
    }
}
