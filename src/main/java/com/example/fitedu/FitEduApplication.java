package com.example.fitedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitEduApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitEduApplication.class, args);
    }

}
