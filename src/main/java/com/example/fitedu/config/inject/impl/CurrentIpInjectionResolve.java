package com.example.fitedu.config.inject.impl;

import com.example.fitedu.common.Constant;
import com.example.fitedu.config.inject.CurrentIp;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.Provider;

@Provider
public class CurrentIpInjectionResolve implements InjectionResolver<CurrentIp> {

    @Inject
    private javax.inject.Provider<ContainerRequestContext> requestContext;

    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> serviceHandle) {
        return requestContext.get().getProperty(Constant.TRACING_IP_ATTRIBUTE_NAME);
    }

    @Override
    public boolean isConstructorParameterIndicator() {
        return false;
    }

    @Override
    public boolean isMethodParameterIndicator() {
        return false;
    }
}
