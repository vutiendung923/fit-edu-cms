package com.example.fitedu.config;

import com.example.fitedu.config.inject.impl.CurrentRequestIdInjectionResolve;
import com.example.fitedu.config.inject.CurrentAuth;
import com.example.fitedu.config.inject.CurrentIp;
import com.example.fitedu.config.inject.CurrentRequestId;
import com.example.fitedu.config.inject.impl.CurrentIpInjectionResolve;
import com.example.fitedu.config.inject.impl.CurrentUserInjectionResolver;
import com.example.fitedu.sys.service.JwtService;
import com.example.fitedu.sys.service.impl.JwtServiceImpl;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;
import javax.ws.rs.ext.Provider;

@Provider
public class BeanConfig extends AbstractBinder {
    @Override
    protected void configure() {
        bind(CurrentUserInjectionResolver.class).to(new TypeLiteral<InjectionResolver<CurrentAuth>>() {
        }).in(Singleton.class);
        bind(CurrentRequestIdInjectionResolve.class).to(new TypeLiteral<InjectionResolver<CurrentRequestId>>() {
        }).in(Singleton.class);
        bind(CurrentIpInjectionResolve.class).to(new TypeLiteral<InjectionResolver<CurrentIp>>() {
        }).in(Singleton.class);
        bind(JwtServiceImpl.class).to(JwtService.class).in(Singleton.class);
    }
}
