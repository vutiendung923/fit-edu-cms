package com.example.fitedu.config;

import com.example.fitedu.config.filter.*;
import com.example.fitedu.exception.mapper.ConstraintViolationExceptionMapper;
import com.example.fitedu.exception.mapper.GenericExceptionMapper;
import com.example.fitedu.sys.controller.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(AccountController.class);
        register(CoursesController.class);
        register(CustomerController.class);
        register(LanguageController.class);
        register(QuestionController.class);

        register(AuthFilter.class);
        register(ClientLoggingFilter.class);
        register(IpFilterImpl.class);
        register(OneTpsImpl.class);
        register(CustomLoggingRequestFilter.class);
        register(CustomLoggingResponseFilter.class);
        register(SwaggerConfig.class);

        register(ConstraintViolationExceptionMapper.class);
        register(GenericExceptionMapper.class);

        register(RolesAllowedDynamicFeature.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
        register(BeanConfig.class);
        register(JsonConfig.class);
    }
}
