package com.example.fitedu.exception;

import javax.ws.rs.NotFoundException;

public class DuplicateExceptionUserName extends NotFoundException {
    public DuplicateExceptionUserName(String userName) {
        super(String.format("Tên đăng nhập  `%s` đã tồn tại, vui lòng chọn tên khác ", userName));
    }
}
