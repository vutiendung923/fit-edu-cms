package com.example.fitedu.exception;

import javax.ws.rs.NotFoundException;

public class NotFoundAccountException extends NotFoundException {
    public NotFoundAccountException() {
        super(String.format("Thông tin đăng nhập không chính xác"));
    }
}
